const assert = require('chai').assert;

const numbersToEnglish = require('../lib/numbersToEnglish');

describe('numbersToEnglish', () => {

    let transform;

    beforeEach(() => {
        transform = numbersToEnglish.transform;
    });

    describe('transform',() => {

        it('method is a function', () => {
            assert.isFunction(transform, 'transform is a function');
        });

        it('throws with no arguments', () => {
            assert.throws(() => transform(), Error, 'undefined is not a number');
        });

        it('throws with bad input', () => {
           assert.throws(() => transform('bad input'), Error, 'bad input is not a number');
        });

        it('converts 0 to Zero', () => {
            assert.equal(transform(0), 'Zero', '0 converts to Zero');
        });

        it('converts 13 to Thirteen', () => {
            assert.equal(transform(13), 'Thirteen', '13 converts to Thirteen');
        });

        it('converts 85 to Eighty five', () => {
           assert.equal(transform(85), 'Eighty five', '85 converts to Eighty five'); 
        });

        it('converts 123 to One hundred and twenty three', () => {
            assert.equal(transform(123), 'One hundred and twenty three',
                '123 converts to One hundred and twenty three');
        });

        it('converts 500 to Five hundred', () => {
            assert.equal(transform(500), 'Five hundred', '500 converts to Five hundred');
        });

        it('converts 5237 to Five thousand two hundred and thirty seven', () => {
            assert.equal(transform(5237), 'Five thousand two hundred and thirty seven', 
                '5237 converts to Five thousand two hundred and thirty seven');
        });

        it('converts 321000 to Three hundred twenty one thousand', () => {
            assert.equal(transform(321000), 'Three hundred and twenty one thousand',
                '321000 converts to Three hundred and twenty one thousand');
        });

        it('converts -9999999 to Negative nine million nine hundred and ninety nine thousand nine hundred and ninety nine', () => {
            assert.equal(transform(-9999999), 'Negative nine million nine hundred and ninety nine thousand nine hundred and ninety nine',
                '-9999999 converts to Negative nine million nine hundred and ninety nine thousand nine hundred and ninety nine');
        });

        it('converts 999999999999 to Nine hundred and ninenty nine billion nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine', () => {
            assert.equal(transform(999999999999), 'Nine hundred and ninety nine billion nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine',
                '999999999999 converts to Nine hundred and ninety nine billion nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine');
        });
    });
});