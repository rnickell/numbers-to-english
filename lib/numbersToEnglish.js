'use strict';

const numsLessThanTwentyWords = [
    '',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
];

const tensWords = [
    '',
    '',
    'twenty',
    'thirty',
    'fourty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety',
];

const ONE_THOUSAND = 1000;
const ONE_MILLION = 1000000;
const ONE_BILLION = 1000000000;
const ONE_TRILLION = 1000000000000;

function getWords(num, wordList) {
    if (num === 0 && !wordList.length) {
        wordList.push( 'zero' );
        return;
    }

    // Don't process the 0 if we're not Zero.
    if (num === 0) {
        return;
    }

    if (Math.sign(num) === -1 && !wordList.length) {
        wordList.push( 'negative' );
        num = Math.abs( num );
    }

    let rem,
        word;

    if (num < 20) {
        word = numsLessThanTwentyWords[ num % 20 ];
        wordList.push( word );
        return;
    }

    if (num < 100) {
        rem = num - Math.floor( num / 10 ) * 10;
        word = tensWords[ parseInt( Math.abs( num / 10 ), 10 ) ];
        wordList.push( word );
    } else if (num < ONE_THOUSAND) {
        rem = num - Math.floor( num / 100 ) * 100;
        word = numsLessThanTwentyWords[ parseInt( Math.abs( num / 100 ), 10 ) ];
        wordList.push( word );
        wordList.push( 'hundred' );
        if (rem !== 0) {
            wordList.push( 'and' );
        }
    } else if (num < ONE_MILLION) {
        rem = num - Math.floor( num / ONE_THOUSAND ) * ONE_THOUSAND;
        const numSegment = parseInt( Math.abs( num / ONE_THOUSAND ), 10 );
        getWords( numSegment, wordList );
        wordList.push( 'thousand' );
    } else if (num < ONE_BILLION) {
        rem = num - Math.floor( num / ONE_MILLION ) * ONE_MILLION;
        const numSegment = parseInt( Math.abs( num / ONE_MILLION ), 10 );
        getWords( numSegment, wordList );
        wordList.push( 'million' );
    } else if (num < ONE_TRILLION) {
        rem = num - Math.floor( num / ONE_BILLION ) * ONE_BILLION;
        const numSegment = parseInt( Math.abs( num / ONE_BILLION ), 10 );
        getWords( numSegment, wordList );
        wordList.push( 'billion' );
    }

    getWords( rem, wordList );
}

function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function transform(num) {
    if (!Number.isFinite(num)) {
        throw Error(`${num} is not a number`);
    }

    const wordList = [];
    getWords( num, wordList );

    return capitalize( wordList.join(' ') );
}

module.exports = {
    transform,
};