# numbers-to-english

## Description

Provides the ability to convert numbers into English words, e.g. `123` -> `One hundred and twenty three`.

### Installation

numbers-to-english was developed against [Node.js](https://nodejs.org/) v8.4.0.

Install the devDependencies to run the tests

```sh
$ cd numbers-to-english
$ npm install
$ npm test
```

To run an example

```sh
$ npm run example
```

### Usage

To use numbers-to-english in your application, first install the dependency

```sh
$ npm install https://bitbucket.org/rnickell/numbers-to-english
```

Then in your Nodejs project

```javascript
const transform = require('numbers-to-english').transform;
const words = transform(2017);
console.log(words); // Two thousand seventeen
```