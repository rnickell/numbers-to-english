const fs = require('fs');
const transform = require('./lib/numbersToEnglish').transform;

const data = fs.readFileSync('./lib/numbersToEnglish.js');
const res = data.toString().split('\n').length;

console.log(`numbers-to-english.js contains ${transform(res)} lines of code.`);
